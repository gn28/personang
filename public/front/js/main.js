document.addEventListener('DOMContentLoaded', function() {
	var swiper = new Swiper('.swiper-container', {
		breakpoints: {
						320: {
								slidesPerView: 1,
								spaceBetween: 30,
						},
			768: {
					slidesPerView: 5,
					spaceBetween: 30,
						},
		},
		slidesPerGroup: 1,
		loop: true,
		loopFillGroupWithBlank: true,
		navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
		},
	});

});
function regFunction() {
	var element = document.getElementById("reg-popup");
	element.classList.add("open");
}
function closeFunction() {
	var element = document.getElementById("reg-popup");
	element.classList.remove("open");
}
function loginFunction() {
	var element = document.getElementById("login-popup");
	element.classList.add("open");
}
function closeloginFunction() {
	var element = document.getElementById("login-popup");
	element.classList.remove("open");
}
function toggleFunction() {
	var element = document.getElementById("mobile-head");
	element.classList.toggle("white");
}
