<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\ArticleUpdateRequest;
use App\Http\Requests\ArticleAjaxUpdateRequest;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $paginationNumber;

    public function __construct()
    {
        $this->paginationNumber = config('values.items_per_page');
    }

    public function index()
    {
        $items = Article::orderByDesc('id')->paginate($this->paginationNumber);

        return view('admin.blog.list', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleStoreRequest $request)
    {
        $sanitized = $request->getSanitized();

        Article::create($sanitized);

        return redirect()->route('admin.blog.list')->with('message', 'Статья создана')->with('type', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Article::find($id);
        return view('admin.blog.edit', [
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleUpdateRequest $request, $id)
    {
        $sanitized = $request->getSanitized();

        $user = Article::find($id);
        $user->update($sanitized);

        return redirect()->route('admin.blog.list')->with('message', 'Запись обновлена')->with('type', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Article::find($id);
        $user->delete();

        return redirect()->route('admin.blog.list')->with('message', 'Запись удалена')->with('type', 'danger');
    }

    public function enableToggle(ArticleAjaxUpdateRequest $request)
    {
        $sanitized = $request->getSanitized();

        $user = Article::find($request->get('id'));
        $user->update($sanitized);

        return response()->json(['success' => true, 'errors' => false]);
    }
}
