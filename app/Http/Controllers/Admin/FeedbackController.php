<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewUpdateRequest;
use Illuminate\Http\Request;
use App\Models\Review;
use App\Enums\ReviewStatus;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $paginationNumber;

    public function __construct()
    {
        $this->paginationNumber = config('values.items_per_page');
    }

    public function index()
    {
        $items = Review::orderByDesc('id')->paginate($this->paginationNumber);

        return view('admin.feedback.list', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Review::find($id);

        return view('admin.feedback.edit',[
            'item' => $item,
            'statuses' => ReviewStatus::getValues()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReviewUpdateRequest $request, $id)
    {
        $sanitized = $request->getSanitized();

        $user = Review::find($id);
        $user->update($sanitized);

        return redirect()->route('admin.feedback.list')->with('message', 'Запись обновлена')->with('type', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Review::find($id);
        $user->delete();

        return redirect()->route('admin.feedback.list')->with('message', 'Запись удалена')->with('type', 'danger');
    }
}
