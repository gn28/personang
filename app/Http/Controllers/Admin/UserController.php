<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserAjaxUpdateRequest;
use App\Enums\UserRole;

class UserController extends Controller
{

    private $paginationNumber;

    public function __construct()
    {
        $this->paginationNumber = config('values.items_per_page');
    }

    public function index()
    {
        $items = User::where('role', UserRole::User)->orderByDesc('id')->paginate($this->paginationNumber);
        return view('admin.users.list', [
            'items' => $items
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::find($id);
        return view('admin.users.edit', [
            'item' => $item
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $sanitized = $request->getSanitized();

        $user = User::find($id);
        $user->update($sanitized);

        return redirect()->route('admin.users.list')->with('message', 'Запись обновлена')->with('type', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('admin.users.list')->with('message', 'Запись удалена')->with('type', 'danger');
    }

    public function subscriptionChange(UserAjaxUpdateRequest $request)
    {
        $sanitized = $request->getSanitized();

        $user = User::find($request->get('id'));
        $user->update($sanitized);

        return response()->json(['success' => true, 'errors' => false]);
    }

}
