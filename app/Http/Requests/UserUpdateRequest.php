<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'town' => 'required'
        ];
    }

    public function getSanitized() : array
    {
        $sanitized = $this->all();

        if (isset($sanitized['subscription_enable']) && $sanitized['subscription_enable'] == 'on') {
            $sanitized['subscription_enable'] = true;
        } else {
            $sanitized['subscription_enable'] = false;
        }

        return $sanitized;
    }
}
