<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleAjaxUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'checked' => 'required'
        ];
    }

    public function getSanitized() : array
    {
        $sanitized = $this->all();

        if ($sanitized['checked'] == 'true') {
            $sanitized['enable'] = true;
        } else {
            $sanitized['enable'] = false;
        }

        return $sanitized;
    }
}
