<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class AdminUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'password' => 'confirmed'
        ];
    }

    public function getSanitized()
    {
        $sanitized = $this->all();

        if ($sanitized['password']) {
            $sanitized['password'] = Hash::make($sanitized['password']);
        } else {
            unset($sanitized['password']);
        }
        return $sanitized;
    }
}
