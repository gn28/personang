<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'publish_date' => 'required|date'
        ];
    }

    public function getSanitized() : array
    {
        $sanitized = $this->all();

        if (isset($sanitized['enable']) && $sanitized['enable'] == 'on') {
            $sanitized['enable'] = true;
        } else {
            $sanitized['enable'] = false;
        }

        return $sanitized;
    }
}
