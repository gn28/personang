<?php

namespace App\Http\Requests;

use App\Enums\UserRole;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class AdminStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed|min:8'
        ];
    }

    public function getSanitized()
    {
        $sanitized = $this->all();
        $sanitized['password'] = Hash::make($sanitized['password']);
        $sanitized['role'] = UserRole::Admin;
        
        return $sanitized;
    }
}
