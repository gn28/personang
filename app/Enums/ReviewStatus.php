<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class ReviewStatus extends Enum implements LocalizedEnum
{
    const Seen =   'seen';
    const Unseen =   'unseen';
}
