@extends('admin.layouts.app')

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Администраторы</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Список администраторов</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            @include('admin.partials.noty')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Админы</strong>
                            <a href="{{ route('admin.admins.create') }}" class="float-right btn-sm btn-success" style=""><i class="fa fa-plus-square-o"></i>
                                Добавить
                            </a>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Имя</th>
                                    <th scope="col">Email</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($items as $item)
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>
                                                <a href="{{ route('admin.admins.edit', $item->id) }}" class="btn btn-success" role="button"><i class="fa fa-edit"></i></a>
                                                <a href="#" class="btn btn-danger" role="button" onclick="event.preventDefault();
                                                    if(confirm('Вы уверены?')){document.getElementById('remove-' + {{$item->id}}).submit();}
                                                    "><i class="fa fa-trash"></i></a>
                                                <form action="{{ route('admin.admins.delete', $item->id) }}" method="POST">
                                                    @csrf
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $items->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- .content -->
@endsection
