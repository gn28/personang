<div class="form-group">
    <label for="name" class=" form-control-label">Имя</label>
    <input type="text" id="name" name="name" placeholder="Введите имя" class="form-control" value="{{ $item->name ?? ''}}">
</div>

<div class="form-group">
    <label for="email" class=" form-control-label">Email</label>
    <input type="text" id="email" name="email" placeholder="Email" class="form-control" value="{{ $item->email ?? ''}}">
</div>

<div class="form-group">
    <label for="password" class=" form-control-label">Новый пароль</label>
    <input type="password" id="password" name="password" placeholder="Пароль" class="form-control" value="">
</div>

<div class="form-group">
    <label for="password_confirmation" class=" form-control-label">Подтверждение пароля</label>
    <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Подтверждение пароля" class="form-control" value="">
</div>
