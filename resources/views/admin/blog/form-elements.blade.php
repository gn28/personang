<div class="form-group">
    <label for="title" class=" form-control-label">Заголовок статьи</label>
    <input type="text" id="title" name="title" placeholder="Введите заголовок" class="form-control" value="{{ $item->title ?? ''}}">
</div>

<div class="form-group">
    <label for="short_description" class=" form-control-label">Краткое описание</label>
    <textarea name="short_description" id="short_description" placeholder="" class="form-control" rows="5">{{ $item->short_description ?? '' }}</textarea>
</div>

<div class="form-group">
    <label for="content" class=" form-control-label">Содержание статьи</label>
    <textarea name="content" id="content" placeholder="" >{{ $item->content ?? '' }}</textarea>
</div>

<div class="form-group">
    <label for="content" class=" form-control-label">Время и дата публикации</label>
    <input class="form-control" id="datepicker" name="publish_date" value="{{ $item->publish_date ?? '' }}">
</div>

<div class="form-group">
    <label for="enable" class=" form-control-label">Активность</label>
    <div>
        <label class="switch switch-default switch-pill switch-success mr-2">
            <input type="checkbox" id="enable" name="enable" class="switch-input" {{ isset($item->enable) ? $item->enable ? 'checked' : '' : 'checked' }}>
            <span class="switch-label"></span>
            <span class="switch-handle"></span>
        </label>
    </div>
</div>



<div class="form-group">
    <label for="seo_title" class=" form-control-label">SEO title</label>
    <input type="text" id="seo_title" name="seo_title" placeholder="Введите SEO title" class="form-control" value="{{ $item->seo_title ?? ''}}">
</div>

<div class="form-group">
    <label for="seo_keywords" class=" form-control-label">SEO keywords</label>
    <input type="text" id="seo_keywords" name="seo_keywords" placeholder="Введите SEO keywords" class="form-control" value="{{ $item->seo_keywords ?? ''}}">
</div>

<div class="form-group">
    <label for="seo_description" class=" form-control-label">SEO description</label>
    <textarea name="seo_description" id="seo_description" placeholder="" class="form-control" rows="5">{{$item->seo_description ?? ''}}</textarea>
</div>
@section('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#content' ), {
                removePlugins: [ 'MediaEmbed', 'EasyImage','ImageUpload'],
            } )
            .then( editor => {
                // console.log( editor );
            } )
            .catch( error => {
                console.error( error );
            } );

        jQuery(function () {
            jQuery('#datepicker').datetimepicker({
                locale: 'ru',
                format: 'YYYY-MM-DD HH:mm',
            });
        });
    </script>
@endsection
