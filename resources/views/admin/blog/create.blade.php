@extends('admin.layouts.app')

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Создание статьи блога</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="{{ route('admin.blog.list') }}">Список статей блога</a></li>
                        <li class="active">Создание новой статьи</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">

        <div class="animated fadeIn">
            @include('admin.partials.noty')
            <div class="row">
                <div class="col-lg-12">
                    <form action="{{ route('admin.blog.store') }}" method="post">
                        <div class="card">
                            <div class="card-header"><strong>Создание</strong></div>
                            <div class="card-body card-block">
                                @csrf
                                @include('admin.blog.form-elements')
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Сохранить
                                </button>
                                <a href="{{ route('admin.blog.list') }}" class="btn btn-danger btn-sm like-link-btn">
                                    <i class="fa fa-ban"></i> Отменить
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if($errors->any())
                {!! implode('', $errors->all('<div class="alert alert-danger" role="alert">:message</div>')) !!}
            @endif
        </div>
    </div> <!-- .content -->
@endsection
