@extends('admin.layouts.app')

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Блог</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Список статей блога</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            @include('admin.partials.noty')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Статьи</strong>
                            <a href="{{ route('admin.blog.create') }}" class="float-right btn-sm btn-success" style=""><i class="fa fa-plus-square-o"></i>
                                Добавить
                            </a>
                        </div>
                        <div class="card-body">

                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Заголовок</th>
                                    <th scope="col">Дата публикации</th>
                                    <th scope="col">Активность</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->publish_date }}</td>
                                        <td>
                                            <label class="switch switch-default switch-pill switch-success mr-2">
                                                <input data-id="{{ $item->id }}" type="checkbox" class="switch-input article-enable" {{ $item->enable ? 'checked' : '' }}>
                                                <span class="switch-label"></span>
                                                <span class="switch-handle"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.blog.edit', $item->id) }}" class="btn btn-success" role="button"><i class="fa fa-edit"></i></a>

                                            <a href="#" class="btn btn-danger" role="button" onclick="event.preventDefault();
                                                if(confirm('Вы уверены?')){document.getElementById('remove-' + {{$item->id}}).submit();};
                                                "><i class="fa fa-trash"></i></a>
                                            <form id="remove-{{$item->id}}" action="{{ route('admin.blog.delete', $item->id) }}" method="POST">
                                                @csrf
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $items->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
            <script>
                jQuery('.article-enable').on('change', function() {
                    let checked = this.checked;
                    let id = jQuery(this).data('id');
                    let token = jQuery('meta[name="csrf-token"]').attr('content');

                    jQuery.ajax({
                        url: '/admin/blog/ajax/enable',
                        method: 'post',
                        dataType: 'json',
                        data: {
                            id: id,
                            checked: checked,
                            _token: token
                        },
                        success: function(data){
                            console.log(data);
                        }
                    })
                });
            </script>
@endsection
