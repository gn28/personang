<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PERSONANG - Admin Panel</title>
    <meta name="description" content="PERSONANG - Admin Panel">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" href="/sufee/apple-icon.png">
    <link rel="shortcut icon" href="/sufee/favicon.ico">
    <link rel="stylesheet" href="/sufee/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/sufee/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/sufee/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="/sufee/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="/sufee/vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="/sufee/vendors/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="/sufee/vendors/datetimepicker-bootstrap-4/build/css/bootstrap-datetimepicker.min.css">

    <link rel="stylesheet" href="/sufee/assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <style>
        .like-link-btn {
            color: white!important;
        }
        .user-image {
            width: 150px;
        }
        .ck-editor__editable_inline {
            min-height: 250px;
        }
    </style>

</head>

<body>


<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./">PERSONANG</a>
            <a class="navbar-brand hidden" href="./">P</a>
        </div>

        @include('admin.partials.left-menu')

    </nav>
</aside><!-- /#left-panel -->

<!-- Left Panel -->

<!-- Right Panel -->

<div id="right-panel" class="right-panel">

    <!-- Header-->
    <header id="header" class="header">

        <div class="header-menu">

            <div class="col-sm-7">
                <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                <div class="header-left">
                </div>
            </div>

            <div class="col-sm-5">
                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="user-avatar rounded-circle" src="/sufee/images/logout.png" alt="User Avatar">
                    </a>

                    <div class="user-menu dropdown-menu">
                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            @csrf
                        </form>
                        <a class="nav-link" href="#" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> Выйти
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </header><!-- /header -->
    <!-- Header-->
    @yield('content')
</div><!-- /#right-panel -->

<!-- Right Panel -->

<script src="/sufee/vendors/jquery/dist/jquery.min.js"></script>
<script src="/sufee/vendors/popper.js/dist/umd/popper.min.js"></script>
<script src="/sufee/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/sufee/assets/js/main.js"></script>


{{--<script src="/sufee/vendors/chart.js/dist/Chart.bundle.min.js"></script>--}}
{{--<script src="/sufee/assets/js/dashboard.js"></script>--}}
{{--<script src="/sufee/assets/js/widgets.js"></script>--}}
<script src="/sufee/vendors/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="/sufee/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<script src="/sufee/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/locale/ru.min.js"></script>
<script src="/sufee/vendors/datetimepicker-bootstrap-4/build/js/bootstrap-datetimepicker.min.js"></script>
<script>
    (function($) {
        "use strict";

        jQuery('#vmap').vectorMap({
            map: 'world_en',
            backgroundColor: null,
            color: '#ffffff',
            hoverOpacity: 0.7,
            selectedColor: '#1de9b6',
            enableZoom: true,
            showTooltip: true,
            values: sample_data,
            scaleColors: ['#1de9b6', '#03a9f5'],
            normalizeFunction: 'polynomial'
        });
    })(jQuery);
</script>
    @yield('scripts');
</body>

</html>
