<div id="main-menu" class="main-menu collapse navbar-collapse">
    <ul class="nav navbar-nav">
        <li class="{{ Route::currentRouteName() == 'admin.dashboard' ? 'active' : '' }}">
            <a href="{{ route('admin.dashboard') }}"> <i class="menu-icon fa fa-dashboard"></i>Панель</a>
        </li>
        <h3 class="menu-title">Меню</h3><!-- /.menu-title -->
        <li class="{{ Route::currentRouteName() == 'admin.users.list' ? 'active' : '' }}">
            <a href="{{ route('admin.users.list') }}"> <i class="menu-icon fa fa-users"></i>Пользователи</a>
        </li>
        <li class="{{ Route::currentRouteName() == 'admin.blog.list' ? 'active' : '' }}">
            <a href="{{ route('admin.blog.list') }}"> <i class="menu-icon fa fa-pencil"></i>Статьи блога</a>
        </li>
        <li class="{{ Route::currentRouteName() == 'admin.feedback.list' ? 'active' : '' }}">
            <a href="{{ route('admin.feedback.list') }}"> <i class="menu-icon fa fa-comment"></i>Обратная связь</a>
        </li>
        <li class="{{ Route::currentRouteName() == 'admin.admins.list' ? 'active' : '' }}">
            <a href="{{ route('admin.admins.list') }}"> <i class="menu-icon fa fa-user"></i>Администраторы</a>
        </li>
        <li class="{{ Route::currentRouteName() == 'admin.settings.index' ? 'active' : '' }}">
            <a href="{{ route('admin.settings.index') }}"> <i class="menu-icon fa fa-cogs"></i>Настройки</a>
        </li>
    </ul>
</div><!-- /.navbar-collapse -->
