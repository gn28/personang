@if($errors->any())
    {!! implode('', $errors->all('<div class="alert alert-danger" role="alert">:message</div>')) !!}
@endif
@if(Session::has('message') && Session::has('type'))
    <div class="alert alert-{{ Session::get('type') }}" role="alert">{{ Session::get('message') }}</div>
@endif
