@extends('admin.layouts.app')

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Редактировие</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="{{ route('admin.users.list') }}">Список пользователей</a></li>
                        <li class="active">Редактирование</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">

        <div class="animated fadeIn">
            @include('admin.partials.noty')
            <div class="row">
                <div class="col-lg-12">
                    <form action="{{ route('admin.users.update', $item->id) }}" method="post">
                        <div class="card">
                            <div class="card-header"><strong>Пользователь</strong></div>
                            <div class="card-body card-block">
                                @csrf
                                <div class="form-group">
                                    @if ($item->photo)
                                        <img class="user-image" src="{{ $item->photo }}">
                                    @else
                                        <img class="user-image" src="/images/default-user.png">
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="name" class=" form-control-label">ФИО</label>
                                    <input type="text" id="name" name="name" placeholder="Введите ФИО" class="form-control" value="{{ $item->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="email" class=" form-control-label">Email</label>
                                    <input type="text" id="email" name="email" placeholder="Введите email" class="form-control" value="{{ $item->email }}">
                                </div>
                                <div class="form-group">
                                    <label for="town" class=" form-control-label">Город</label>
                                    <input type="text" id="town" name="town" placeholder="Введите город" class="form-control" value="{{ $item->town }}">
                                </div>
                                <div class="form-group">
                                    <label for="company_name" class=" form-control-label">Название организации</label>
                                    <input type="text" id="company_name" name="company_name" placeholder="Введите название организации" class="form-control" value="{{ $item->company_name }}">
                                </div>
                                <div class="form-group">
                                    <label for="unp" class=" form-control-label">УНП</label>
                                    <input type="text" id="unp" name="unp" placeholder="Введите УНП" class="form-control" value="{{ $item->unp }}">
                                </div>

                                <div class="form-group">
                                    <label for="commercials" class=" form-control-label">Коммерческие объекты</label>
                                    <textarea name="commercials" id="commercials" placeholder="" class="form-control" rows="5">{{$item->commercials}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="subscription_enable" class=" form-control-label">Требование подписки</label>
                                    <div>
                                        <label class="switch switch-default switch-pill switch-success mr-2">
                                            <input type="checkbox" id="subscription_enable" name="subscription_enable" class="switch-input" {{ $item->subscription_enable ? 'checked' : '' }}>
                                            <span class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-facebook-square"></i></div>
                                        <input type="text" id="fb" name="fb" class="form-control" value="{{ $item->fb }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-vk"></i></div>
                                        <input type="text" id="vk" name="vk" class="form-control" value="{{ $item->vk }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-twitter"></i></div>
                                        <input type="text" id="tw" name="tw" class="form-control" value="{{ $item->tw }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-instagram"></i></div>
                                        <input type="text" id="instagram" name="instagram" class="form-control" value="{{ $item->instagram }}">
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Сохранить
                                </button>
                                <a href="{{ route('admin.users.list') }}" class="btn btn-danger btn-sm like-link-btn">
                                    <i class="fa fa-ban"></i> Отменить
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if($errors->any())
                {!! implode('', $errors->all('<div class="alert alert-danger" role="alert">:message</div>')) !!}
            @endif
        </div>
    </div> <!-- .content -->
@endsection
