@extends('admin.layouts.app')

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Пользователи</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Список пользователей</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            @include('admin.partials.noty')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Пользователи</strong>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">ФИО</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Город</th>
                                    <th scope="col">Организация</th>
                                    <th scope="col">Требование подписки</th>
                                    <th scope="col">Подписка до</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <th scope="row">{{ $item->id }}</th>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->town }}</td>
                                        <td>{{ $item->company_name }}</td>
                                        <td>
                                            <label class="switch switch-default switch-pill switch-success mr-2">
                                                <input data-id="{{ $item->id }}" type="checkbox" class="switch-input subscription-enable" {{ $item->subscription_enable ? 'checked' : '' }}>
                                                <span class="switch-label"></span>
                                                <span class="switch-handle"></span>
                                            </label>
                                        </td>
                                        <td>{{ $item->subscrition_until }}</td>
                                        <td>
                                            <a href="{{ route('admin.users.edit', $item->id) }}" class="btn btn-success" role="button"><i class="fa fa-edit"></i></a>

                                            <a href="#" class="btn btn-danger" role="button" onclick="event.preventDefault();
                                                    if(confirm('Вы уверены?')){document.getElementById('remove-' + {{$item->id}}).submit();};
                                                    "><i class="fa fa-trash"></i></a>
                                            <form id="remove-{{$item->id}}" action="{{ route('admin.users.delete', $item->id) }}" method="POST">
                                                @csrf
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $items->links() }}
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('scripts')
            <script>
                jQuery('.subscription-enable').on('change', function() {
                    let checked = this.checked;
                    let id = jQuery(this).data('id');
                    let token = jQuery('meta[name="csrf-token"]').attr('content');

                    jQuery.ajax({
                        url: '/admin/users/ajax/sub-change',
                        method: 'post',
                        dataType: 'json',
                        data: {
                            id: id,
                            checked: checked,
                            _token: token
                        },
                        success: function(data){
                            console.log(data);
                        }
                    })
                    // console.log(id);
                });
            </script>
@endsection
