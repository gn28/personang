@extends('admin.layouts.app')

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Редактировие</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="{{ route('admin.feedback.list') }}">Список обратной связи</a></li>
                        <li class="active">Редактирование</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">

        <div class="animated fadeIn">
            @include('admin.partials.noty')
            <div class="row">
                <div class="col-lg-12">
                    <form action="{{ route('admin.feedback.update', $item->id) }}" method="post">
                        <div class="card">
                            <div class="card-header"><strong>Обратная связь</strong></div>
                            <div class="card-body card-block">
                                @csrf

                                <div class="form-group">
                                    <label for="name" class=" form-control-label">Имя</label>
                                    <input type="text" id="name" name="name" placeholder="" class="form-control" value="{{ $item->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="email" class=" form-control-label">Email</label>
                                    <input type="text" id="email" name="email" placeholder="" class="form-control" value="{{ $item->email }}">
                                </div>
                                <div class="form-group">
                                    <label for="phone" class=" form-control-label">Телефон</label>
                                    <input type="text" id="phone" name="phone" placeholder="" class="form-control" value="{{ $item->phone }}">
                                </div>

                                <div class="form-group">
                                    <label for="content" class=" form-control-label">Содержание</label>
                                    <textarea name="content" id="content" placeholder="" class="form-control" rows="5">{{ $item->content }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="status" class=" form-control-label">Статус</label>
                                    <select name="status" data-placeholder="" class="form-control standardSelect" tabindex="1">
                                        @foreach ($statuses as $status)
                                            <option value="{{ $status }}" {{ $status == $item->status ? 'selected' : '' }}>{{ __('enum.' . $status) }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Сохранить
                                </button>
                                <a href="{{ route('admin.feedback.list') }}" class="btn btn-danger btn-sm like-link-btn">
                                    <i class="fa fa-ban"></i> Отменить
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if($errors->any())
                {!! implode('', $errors->all('<div class="alert alert-danger" role="alert">:message</div>')) !!}
            @endif
        </div>
    </div> <!-- .content -->
@endsection
