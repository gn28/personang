<!DOCTYPE html>
<html lang="ru"><head>
    <meta charset="utf-8">
    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta property="og:title" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">

    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900|Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/front/css/main.css">
    <link rel="stylesheet" href="/front/css/swiper-bundle.min.css">
</head>
<body>
<header>

    <div class="container mobile-hidden">
        <div>
            <a class="logo" href="/">PERSONANG</a>
        </div>
        <div>
            <ul class="menu">
                <li>
                    <a href="#">ГЛАВНАЯ</a>
                </li>
                <li>
                    <a href="#">БЛОГ</a>
                </li>
                <li>
                    <a href="#">О НАС</a>
                </li>
                <li>
                    <a href="#">КОНТАКТЫ</a>
                </li>
            </ul>
        </div>
        <div class="right">
            <a class="btn-black" href="#">ЧЕРНЫЙ СПИСОК</a>
            <a class="btn-red" href="#">ЛИЧНЫЙ КАБИНЕТ</a>
        </div>
    </div>

    <div class="container desc-hidden" id="mobile-head">

        <div class="hamburger-menu">
            <input id="menu__toggle" type="checkbox" />
            <label class="menu__btn" for="menu__toggle" onclick="toggleFunction()">
                <span></span>
            </label>
            <ul class="menu__box">
                <li><a class="menu__item" href="#">Главная</a></li>
                <li><a class="menu__item" href="#">Блог</a></li>
                <li><a class="menu__item" href="#">О нас</a></li>
                <li><a class="menu__item" href="#">Контакты</a></li>
            </ul>
        </div>

        <div>
            <a class="logo zindex" href="/">PERSONANG</a>
        </div>
        <div class="right">
            <a class="btn-red zindex" href="#">ЛИЧНЫЙ КАБИНЕТ</a>
        </div>
    </div>

</header>
<div class="home-block">
    <div class="container">
        <h1>PERSONANG</h1>
        <p>Каталог компаний, учреждений и физических лиц,</br>которые тем или иным образом нарушают действующее законодательство Росийской Федераци либо общепринятые нормы морали.</p>
        <div>
            <a href="#" class="btn-white" onclick="loginFunction()">ВОЙТИ</a>
            <a href="#" id="registration" class="btn-white" onclick="regFunction()">ЗАРЕГИСТРИРОВАТЬСЯ</a>
        </div>
    </div>
</div>
<div class="container home-bottom">
    <h2>С НАМИ УЖЕ РАБОТАЮТ</h2>
    <p>Каталог компаний, учреждений и физических лиц, которые тем или иным образом нарушают</p>
</div>

<div class="container container-slider">
    <div class="main-slider">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="/front/img/partner-2.png"></div>
                <div class="swiper-slide"><img src="/front/img/partner-1.png"></div>
                <div class="swiper-slide"><img src="/front/img/partner-2.png"></div>
                <div class="swiper-slide"><img src="/front/img/partner-1.png"></div>
                <div class="swiper-slide"><img src="/front/img/partner-2.png"></div>
                <div class="swiper-slide"><img src="/front/img/partner-1.png"></div>
                <div class="swiper-slide"><img src="/front/img/partner-2.png"></div>
                <div class="swiper-slide"><img src="/front/img/partner-1.png"></div>
            </div>
            <!-- Add Arrows -->

        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
<div class="popup" id="reg-popup">
    <div class="main-form">
        <h3>Регистрация</h3>
        <p>Это займет всего 30 секунд!</p>
        <form id="register-form" action="{{ route('register') }}" method="post">
            @csrf
            <input type="text" name="name" placeholder="ФИО *" >
            <input type="text" name="town" placeholder="Ваш город *" >
            <input type="email" name="email" placeholder="Email *" >
            <input type="password" name="password" placeholder="Пароль *" >
            <input type="password" name="password_confirmation" placeholder="Повторите пароль *" >
            <textarea placeholder="Коммерческие объекты *"></textarea>
            <div class="checkbox-block">
                <input type="checkbox" checked>
                <span>Я соглашаюсь на обработку моих персональных данных и с <a href="">правилами работы</a> сервиса</span>
            </div>
            <div class="but-block">
                <button class="btn-black" type="button" onclick="event.preventDefault();
                    document.getElementById('register-form').submit();
                    ">ЗАРЕГИСТРИРОВАТЬСЯ</button>
{{--                <button class="btn-black" type="button">ВОЙТИ</button>--}}
            </div>
            <div class="close" onclick="closeFunction()">x</div>
        </form>
    </div>
</div>

<div class="popup" id="login-popup">
    <div class="main-form">
        <h3>Войти</h3>
        <p>Личный кабинет</p>
        <form id="login-form" action="{{ route('login') }}" method="post">
            @csrf
            <input type="email" name="email" placeholder="Email *" >
            <input type="password" name="password" placeholder="Пароль *" >
            <div class="but-block">
                <button class="btn-black" type="button" onclick="event.preventDefault();
                    document.getElementById('login-form').submit();
                    ">ВОЙТИ</button>
            </div>
            <div class="close" onclick="closeloginFunction()">x</div>
        </form>
    </div>
</div>

<footer>
    <div class="copyright container">
        <p>PERSONANG 2021 | НАПИСАТЬ НАМ EMAIL</p>
    </div>
</footer>
<script src="/front/js/swiper-bundle.min.js"></script>
<script src="/front/js/main.js"></script>
</body>
</html>
