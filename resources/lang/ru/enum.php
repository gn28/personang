<?php

use App\Enums\ReviewStatus;

return [
    ReviewStatus::class => [
        ReviewStatus::Seen => 'просмотрено',
        ReviewStatus::Unseen => 'не просмотрено'
    ],

    'seen' => 'просмотрено',
    'unseen' => 'не просмотрено'
];
