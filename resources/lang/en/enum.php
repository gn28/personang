<?php

use App\Enums\ReviewStatus;

return [
    ReviewStatus::class => [
        ReviewStatus::Seen => 'seens',
        ReviewStatus::Unseen => 'unseen'
    ],

    'seen' => 'seen',
    'unseen' => 'unseen'
];
