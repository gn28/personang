const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

let api = mix.js('resources/js/app.js', 'public/js').postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer')
]).js([
        '/sufee/vendors/bootstrap/dist/js/bootstrap.min.js',
        '/sufee/assets/js/main.js',
        '/sufee/vendors/chart.js/dist/Chart.bundle.min.js',
        '/sufee/assets/js/dashboard.js',
        '/sufee/assets/js/widgets.js',
        '/sufee/vendors/jqvmap/dist/jquery.vmap.min.js',
        '/sufee/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js',
        '/sufee/vendors/jqvmap/dist/maps/jquery.vmap.world.js'
    ], 'public/js/dashboard.js');
