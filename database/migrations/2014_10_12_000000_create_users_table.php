<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\UserRole;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('role', UserRole::getValues())->default(UserRole::User);
            $table->rememberToken();
            $table->string('town')->nullable();
            $table->string('company_name')->nullable();
            $table->string('unp')->nullable();
            $table->string('fb')->nullable();
            $table->string('vk')->nullable();
            $table->string('tw')->nullable();
            $table->string('instagram')->nullable();
            $table->text('commercials')->nullable();
            $table->string('photo')->nullable();
            $table->boolean('subscription_enable')->default(1);
            $table->timestamp('subscription_until')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
