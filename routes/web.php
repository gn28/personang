<?php

use Illuminate\Support\Facades\Route;
use App\Enums\UserRole;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'App\Http\Controllers\Front'], function() {
    Route::get('/', [
        'uses' => 'MainController@index',
        'as' => 'front.main.index'
    ]);

    Route::group(['middleware' => ['auth', 'can:' . UserRole::User]], function() {
        Route::get('/hh', function() {
            return 22;
        });
    });


    Route::get('/black-list', function () {
        dd(312);
    });
});

Route::group(['middleware' => ['auth', 'can:' . UserRole::Admin], 'namespace' => 'App\Http\Controllers\Admin', 'prefix' => 'admin'], function() {

    Route::get('/dashboard', [
        'uses' => 'DashboardController@index',
        'as' => 'admin.dashboard'
    ]);

    Route::group(['prefix' => 'users'], function() {
        Route::get('/list', [
            'uses' => 'UserController@index',
            'as' => 'admin.users.list'
        ]);
        Route::get('/{id}/edit', [
            'uses' => 'UserController@edit',
            'as' => 'admin.users.edit'
        ]);
        Route::post('/{id}/update', [
           'uses' => 'UserController@update',
           'as' => 'admin.users.update'
        ]);
        Route::post('/{id}/delete', [
           'uses' => 'UserController@destroy',
           'as' => 'admin.users.delete'
        ]);
        Route::post('/ajax/sub-change', [
           'uses' => 'UserController@subscriptionChange',
           'as' => 'admin.users.ajax.subscription.change'
        ]);
    });

    Route::group(['prefix' => 'blog'], function() {
        Route::get('/list', [
            'uses' => 'BlogController@index',
            'as' => 'admin.blog.list'
        ]);
        Route::get('/create', [
            'uses' => 'BlogController@create',
            'as' => 'admin.blog.create'
        ]);
        Route::post('/store', [
            'uses' => 'BlogController@store',
            'as' => 'admin.blog.store'
        ]);
        Route::get('/{id}/edit', [
            'uses' => 'BlogController@edit',
            'as' => 'admin.blog.edit'
        ]);
        Route::post('/{id}/update', [
            'uses' => 'BlogController@update',
            'as' => 'admin.blog.update'
        ]);
        Route::post('/{id}/delete', [
            'uses' => 'BlogController@destroy',
            'as' => 'admin.blog.delete'
        ]);
        Route::post('/ajax/enable', [
            'uses' => 'BlogController@enableToggle',
            'as' => 'admin.blog.ajax.enable.toggle'
        ]);
    });

    Route::group(['prefix' => 'feedback'], function() {
        Route::get('/list', [
            'uses' => 'FeedbackController@index',
            'as' => 'admin.feedback.list'
        ]);
        Route::get('/{id}/edit', [
            'uses' => 'FeedbackController@edit',
            'as' => 'admin.feedback.edit'
        ]);
        Route::post('/{id}/update', [
            'uses' => 'FeedbackController@update',
            'as' => 'admin.feedback.update'
        ]);
        Route::post('/{id}/delete', [
            'uses' => 'FeedbackController@destroy',
            'as' => 'admin.feedback.delete'
        ]);
    });

    Route::group(['prefix' => 'admins'], function() {
        Route::get('/list', [
            'uses' => 'AdminController@index',
            'as' => 'admin.admins.list'
        ]);
        Route::get('/create', [
            'uses' => 'AdminController@create',
            'as' => 'admin.admins.create'
        ]);
        Route::post('/store', [
            'uses' => 'AdminController@store',
            'as' => 'admin.admins.store'
        ]);
        Route::get('/{id}/edit', [
            'uses' => 'AdminController@edit',
            'as' => 'admin.admins.edit'
        ]);
        Route::post('/{id}/update', [
            'uses' => 'AdminController@update',
            'as' => 'admin.admins.update'
        ]);
        Route::post('/{id}/delete', [
            'uses' => 'AdminController@destroy',
            'as' => 'admin.admins.delete'
        ]);
    });

    Route::group(['prefix' => 'settings'], function() {
        Route::get('/', [
            'uses' => 'SettingsController@index',
            'as' => 'admin.settings.index'
        ]);
    });
});





Route::group(['middleware' => ['auth', 'can:admins']], function() {
    Route::get('/gg', function() {
        return 11;
    });
});

require __DIR__.'/auth.php';
